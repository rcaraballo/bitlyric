from .models import Musica


def ult_canciones(request):
    """
    Enviara las ultimas 20 Canciones agregadas a cualquier template que
    se desee con solo agregar en el template la logica para recorrer la lista
    de la variable que se le envia <ult_cancion>.
    """

    context = {'ult_canciones': Musica.objects.all()[:20:-1]}
    print "CONTEXT ENVIADO"

    return context
