from django.conf.urls import url

from .views import Index, Lyric, Artist, Search

urlpatterns = [

    url(r'^$', Index.as_view(), name='index'),

    url(r'^artista/(?P<artist>[\w\-]+)/$', Artist.as_view(), name='artista'),

    url(r'^(?P<artista>[\w\-]+)/cancion/(?P<cancion>[\w\-]+)/$',
        Lyric.as_view(), name='cancion')
    ,
    url(r'^search$', Search.as_view(), name='search'),

]

"""
Esto solo funciona en ambiente de desarrollo para poder servir los
archivos que los usuario van subiendo a la web.
"""
from django.conf import settings

if settings.DEBUG == True:
     urlpatterns += (
         url(r'media/(?P<path>.*)$', 'django.views.static.serve',
             {'document_root': settings.MEDIA_ROOT}
         ),
     )