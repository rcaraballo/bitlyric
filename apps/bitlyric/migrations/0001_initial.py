# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Artistas',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('img', models.ImageField(upload_to=b'artistas')),
                ('slug', models.SlugField()),
            ],
        ),
        migrations.CreateModel(
            name='Musica',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=350)),
                ('link', models.CharField(max_length=350)),
                ('letra', models.TextField()),
                ('slug', models.SlugField()),
                ('artistas', models.ManyToManyField(to='bitlyric.Artistas')),
            ],
        ),
    ]
