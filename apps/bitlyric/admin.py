from django.contrib import admin

from .models import Artistas, Musica


admin.site.register(Artistas)
admin.site.register(Musica)