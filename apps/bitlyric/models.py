from django.db import models
from django.utils.text import slugify


class Artistas(models.Model):
    name = models.CharField(max_length=150)
    img = models.ImageField(upload_to='artistas', blank=True, null=True)
    slug = models.SlugField(editable=False)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
        
        super(Artistas, self).save(*args, **kwargs)


class Musica(models.Model):
    title = models.CharField(max_length=350)
    link = models.CharField(max_length=350)
    letra = models.TextField()
    artistas = models.ManyToManyField(Artistas)
    slug = models.SlugField(editable=False)

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)

        if self.link.startswith('http'):
            self.link = self.link[self.link.find('=')+1 :: ]

        super(Musica, self).save(*args, **kwargs)