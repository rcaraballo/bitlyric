from django.shortcuts import render
from django.views.generic import TemplateView

from apps.bitlyric.models import Artistas, Musica


class Index(TemplateView):
    template_name = 'bitlyric/index.html'


class Artist(TemplateView):
    template_name = 'bitlyric/artist_page.html'

    def get_context_data(self, **kwargs):
        context = super(Artist, self).get_context_data(**kwargs)

        # agarra el valor que viene en la variable artista de la URL
        artista = Artistas.objects.get(slug=kwargs['artist'])

        # para poder acceder a las propiedades del artista en el template
        # debes usar la variable {{ artist }}, si lo deseas cambiarlo
        # debes de modificar en context['<nombre>'] donde <nombre> es lo
        # que usaras para mostrarlo en el template si lo camcbias
        # recuerda que tambien debes cambiarlos en los templates.
        context['artist'] = artista

        # filtra todas las canciones por el valor de la variable artista
        canciones = Musica.objects.filter(artistas=artista)

        # para poder acceder a las propiedades del artista en el template
        # debes usar la variable {{ canciones }}, si lo deseas puedes cambiarlo
        # recuerda que tambien debes cambiarlos en los templates.
        context['canciones'] = canciones

        return context


class Lyric(TemplateView):
    template_name = 'bitlyric/lyric_page.html'

    def get_context_data(self, **kwargs):
        context = super(Lyric, self).get_context_data(**kwargs)

        # agarra el valor que viene en la variable cancion de la URL
        cancion = Musica.objects.get(slug=kwargs['cancion'])
        context['cancion'] = cancion

        # agarra el valor que viene en la variable artista de la URL
        artista = Artistas.objects.get(slug=kwargs['artista'])
        context['artist'] = artista

        context['canciones'] = Musica.objects.filter(artistas=artista)

        return context


class Search(TemplateView):
    template_name = 'bitlyric/search.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        context['searched'] =  request.GET['music']

        canciones_relacionadas = Musica.objects.filter(title__contains=\
                                                       context['searched'])

        context["canciones"] = canciones_relacionadas


        return render(request, 'bitlyric/search.html', context )