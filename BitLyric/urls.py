from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [

    url(r'^', include('apps.bitlyric.urls', namespace="bitlyric",
                      app_name='bitlyric')),

    url(r'^admin/', include(admin.site.urls)),
]
